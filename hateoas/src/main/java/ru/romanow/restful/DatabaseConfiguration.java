package ru.romanow.restful;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by romanow on 18.10.16
 */
@Configuration
@EnableJpaRepositories
public class DatabaseConfiguration {}